***

Im [Archiv](archiv.html) finden Sie Meldungen der Vergangenheit.

***

## Anschrift:
Connollystrasse 32, 80809 München

### Geschäftsstelle:
Helene-Mayer-Ring 31, 80809 München, [Google maps](https://goo.gl/maps/RRDFCmYQwGM2)  
Telefon und Telefax: (089) 351 96 64  
E-Mail: <info@usc-muenchen.de>  
Öffnungszeiten: Dienstag 16.00 - 20.00 Uhr (ausgenommen Schulferien)
