---
layout: post
title: Kartenleser in der Geschäftstelle
---
Die Geschäftstelle verfügt seit kurzem über einen Kartenleser!  
Kurse, etc. können daher statt in bar auch mit EC-Karte bezahlt werden.  
