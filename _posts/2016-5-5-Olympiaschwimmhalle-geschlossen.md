---
layout: post
title: Olympiaschwimmhalle geschlossen 9.-30. Mai
---
Liebe Schwimmfreunde, liebe Eltern, liebe Kinder,  
**die Olympiaschwimmhalle ist vom 9.05.2016 - 30.05.2016 aufgrund der Sanierungsarbeiten geschlossen.**  
Die Kurseinheit, die wir deswegen verlieren werden wir ans Ende der Kursreihe legen, sodass unsere 15 Termine stattfinden können.  
Ich wünsche von Herzen eine schöne Zeit und freue mich schon auf die nächsten Schwimmstunden, mit freundlichen Grüßen,
Elias Maurides, Chefschwimmtrainer/Leiter des USC Kinderschwimmen.
