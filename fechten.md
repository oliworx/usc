---
layout: abteilung
title: Fechten
email: fechten@usc-muenchen.de
homepage: http://www.usc-muenchen.de/Fechten/
---
## Ansprechpartner

### ABTEILUNGSLEITER

Richard May<br>
    Tel.: 0160 - 5 56 31 02

### SPORTWART

Matthias Gehlert<br>
    Tel.: 0173 - 7 48 39 57

### Schriftwart

Hans Krodel<br>
    Tel.: 0172 - 9 80 54 56

## Historie

Gr&uuml;ndungsjahr: 1963<br>
Mitgliederzahl: 75

## Personenkreis

Jugendliche, Studierende und Erwachsene

## Beitrag

&euro;&nbsp;140.-/Jahr f&uuml;r Erwachsene<br>
&euro;&nbsp;100.-/Jahr f&uuml;r Sch&uuml;ler,
Studierende, Wehrpflichtige<br>
&euro;&nbsp;100.-/Jahr f&uuml;r Passive,
Zweitmitgliedschaften

## Trainingszeiten und -orte

Fechthalle der ZHS, Connollystr. 32, 80809 M&uuml;nchen<br>

Kooperatives Training mit dem Hochschulsport:

<table>

  <colgroup><col width="150"><col width="75"><col width="150"></colgroup> <tbody>

<tr>

   <td> </td>

<td>Montag</td>

<td>18.30-21.30 Uhr</td>

</tr>

<tr>

<td> </td>

<td>Dienstag</td>

<td>18.30-21.30 Uhr</td>

</tr>

<tr>

<td> </td>

<td>Mittwoch</td>

<td>18.30-21.30 Uhr</td>

</tr>

<tr>

<td> </td>

<td>Donnerstag</td>

<td>18.00-21.30 Uhr</td>

</tr>

</tbody>
</table>

Einzelheiten siehe aktuelles Programmheft des Hochschulsports
M&uuml;nchen. Kursbeginn jeweils zu Semesteranfang, d.h. Anfang
November bzw. Mai.

## Spielbetrieb

Teilnahme an nationalen und internationalen Aktiven- und Seniorenturnieren

## WETTKAMPFBETRIEB

Teilnahme an den Ranglistenturnieren des Bayerischen
    Fechterverbandes und des Deutschen Fechterbundes in den Einzel- und
    Mannschaftswettbewerben (Degen und Florett).


