---
layout: page
title: Kinderschwimmen
---

Schon seit Jahrzehnten führt der USC München Kinderschwimmkurse durch. Die Ausbildung erfolgt in verschiedenen Gruppen, von der ersten Wassergewöhnung über das Erlernen der verschiedenen Schwimmtechniken bis hin zum Erwerb der Schwimmabzeichen - vom Seepferdchen bis zum Gold-Abzeichen.

**Die Einschreibung für die Kinderschwimmkurse im Sommersemester 2016 beginnt am 2. Februar 2016.**

Die Kurse beginnen ab 1. März 2016. 

Hier ist das [Programm](http://www.usc-muenchen.de/htm/USC_M%C3%BCnchen_Schwimmprogramm_SS_2016.pdf) und das [Anmeldeformular](http://www.usc-muenchen.de/htm/Anmeldung_Sommersemester_2016.pdf).
