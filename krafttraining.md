---
layout: page
title: Krafttraining
---

Für alle USC-Mitglieder wird ein Krafttraining angeboten. Es findet statt:

**Di. 18.30 - 20.00**

auf der Empore der Leichtathletik-Halle.
