---
layout: abteilung
title: Leichtathletik
email: leichtathletik@usc-muenchen.de
homepage: http://www.usc-leichtathletik.de/
---
## Ansprechpartner

### ABTEILUNGSLEITER
Max Weidmann  
Tel.: 015771427615  mail: weidis@web.de

### WEITERE ANSPRECHPARTNER
Karsten Kunz (Finanzen) mail: carsten.kunz@gmx.net  
Betina Gabler  Tel.: 089 / 351 78 91  mail: Betina.Gabler@lg-swm.de

## Historie

Gründungsjahr: 1962

## Personenkreis
Schüler, Jugendliche, Studierende und Erwachsene

## Beitrag

Aufnahmegebühr: 15 Euro  
Kinder/Jugendliche bis 14 Jahre: 96 Euro/Jahr  
Jugendliche/Studenten bis 25 Jahre: 108 Euro/Jahr  
Erwachsene: 144 Euro/Jahr  
zweites Kind: 90 Euro/Jahr  
drittes Kind ist beitragsfrei  
Familie: 240 Euro﻿/Jahr  
Senioren (ab 65 Jahre): 108 Euro/Jahr  
Seniorenehepaare: 168/Jahr  

(USC München e.V., Abteilung Leichtathletik; Konto-Nr. 90-197997 BLZ 70150000 Stadtsparkasse München)

## Trainingszeiten und -orte
Leichtathletikhalle und Freigelände der ZHS, Connollystr. 32, 80809 München

Mo 18.00-20.00 Uhr  
Mi 17.00-18.30 Uhr  
Do 18.00-20.00 Uhr  
Fr 16.00-19.00 Uhr  

## WETTKAMPFBETRIEB

Wettkämpfe finden im Sommer und im Winter statt. Je nach Qualifikation werden die Teilnehmerlnnen auch für auswärtige höherrangige Wettkämpfe gemeldet.

## TERMINE SONSTIGER VERANSTALTUNGEN

[www.lg-swm.de](http://www.lg-swm.de)
