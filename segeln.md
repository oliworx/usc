---
layout: abteilung
title: Segeln
email: segeln@usc-muenchen.de
homepage: http://segeln.usc-muenchen.de/
---

<h2>Ansprechpartner</h2>

<h3>ABTEILUNGSLEITER</h3>
<p>Klaus Sarzio, Heimstättenweg 2,  82049 Pullach<br>
Tel. 0179 / 2440810</p>

<h3>WEITERER ANSPRECHPARTNER</h3>
<p>Martin Renn, Langbehnstr. 12, 80689 München<br>
Tel.: 089 / 70 56 60</p>

<h3>SCHRIFTFÜHRER</h3>
<p>Herbert Thiele, München</p>

<h2>Historie</h2>
<p>Gr&uuml;ndungsjahr: 1964<br>
Mitgliederzahl: 350</p>

<h2>PERSONENKREIS</h2>
<p>Studierende und Erwachsene aus dem Münchner Hochschulbereich</p>

<h2>BEITRAG</h2>
<p>EUR&nbsp;&nbsp; 50,- Aufnahmegeb&uuml;hr<br>
EUR&nbsp;&nbsp; 80,-/Jahr f&uuml;r Erwachsene<br>
EUR&nbsp;&nbsp; 50,-/Jahr f&uuml;r vollimatrikulierte Studierende bis zur Vollendung des 27. Lebensjahres, Kinder und Jugendliche, Ehegatten von Mitgliedern und f&ouml;rdernde Mitglieder</p>

<h2>TRAININGSZEITEN UND -ORTE</h2>
<p>im SS: Wassersportplatz des Zentralen Hochschulsports der TU München in Starnberg (nach Vereinbarung)</p>

<h2>SEGELBETRIEB</h2>
<p>Segelbetrieb in Starnberg auf dem Gelände des Segelzentrums der TU München</p>

<h2>WETTKAMPFBETRIEB</h2>
<p>Teilnahme an Regatten nach Ausschreibung</p>
